package uk.bot_by.cuttly_bot.cuttly;

import feign.slf4j.Slf4jLogger;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import io.github.resilience4j.ratelimiter.RateLimiter;
import io.github.resilience4j.retry.Retry;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.bot_by.util.MessageBundle;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static feign.Logger.Level.BASIC;

public class CuttlyHelper {

	private static final String API_KEY = "key";
	private static final String CUTTLY = "cuttly";
	private static final String CUTTLY_API_KEY_IS_NULL = "Cuttly API key cannot be null";
	private static final Cuttly CUTTLY_FALLBACK = (parameters) -> "{ url: { status: -1 }}";
	private static final String CUTTLY_LOCATOR = "https://cutt.ly/api/api.php";
	private static final String LINK = "url";
	private static final String NAME = "name";
	private static final String SHORT = "short";
	private static final String SHORT_LINK = "shortLink";
	private static final String SLASH = "/";
	private static final String STATISTICS = "stats";
	private static final String STATUS = "status";
	private static final String STATUS_MESSAGE = "statusMessage";
	private static final String UNKNOWN_STATUS = "unknownStatus";

	private final Cuttly cuttly;
	private final String cuttlyKey;
	private final Logger logger;
	private final MessageBundle messageBundle;

	public CuttlyHelper(@NotNull String cuttlyKey, @NotNull String defaultLanguage) {
		Objects.requireNonNull(cuttlyKey, CUTTLY_API_KEY_IS_NULL);

		this.cuttlyKey = cuttlyKey;
		logger = LoggerFactory.getLogger(getClass());
		messageBundle = MessageBundle.getInstance(getClass()).getBundle(defaultLanguage);

		FeignDecorators decorators = FeignDecorators.builder()
				.withRateLimiter(RateLimiter.ofDefaults(CUTTLY))
				.withRetry(Retry.ofDefaults(CUTTLY))
				.withCircuitBreaker(CircuitBreaker.ofDefaults(CUTTLY))
				.withFallback(CUTTLY_FALLBACK)
				.build();
		cuttly = Resilience4jFeign.builder(decorators)
				.logger(new Slf4jLogger())
				.logLevel(BASIC)
				.target(Cuttly.class, CUTTLY_LOCATOR);
	}

	public JSONObject getShortLink(@NotNull String fullLink, @Nullable String language, @Nullable String name) {
		Objects.requireNonNull(fullLink, "Full link should be not null");

		Map<String, String> parameters = getParameters(SHORT, fullLink);

		if (null != name) {
			parameters.put(NAME, name);
		}

		JSONObject response = new JSONObject(getCuttly().get(parameters));
		int status = response.getJSONObject(LINK).getInt(STATUS);

		response.getJSONObject(LINK).put(STATUS_MESSAGE, getStatusMessage(SHORT, status, language));
		if (logger.isDebugEnabled()) {
			logger.debug("getShortLink: full link {}, status {}, short link {}", fullLink, status,
					response.getJSONObject(LINK).optString(SHORT_LINK));
		}

		return response;
	}

	public JSONObject getStatistics(@NotNull String shortLink, @Nullable String language) {
		Objects.requireNonNull(shortLink, "Short link should be not null");

		JSONObject response = new JSONObject(getCuttly().get(getParameters(STATISTICS, shortLink)));
		int status = response.getJSONObject(STATISTICS).getInt(STATUS);

		response.getJSONObject(STATISTICS).put(STATUS_MESSAGE, getStatusMessage(STATISTICS, status, language));
		logger.debug("getStatistics: short link {}, status {}", shortLink, status);

		return response;
	}

	protected Cuttly getCuttly() {
		return cuttly;
	}

	protected Map<String, String> getParameters(@NotNull String key, @NotNull String value) {
		Map<String, String> parameters = new HashMap<>(Collections.singletonMap(API_KEY, cuttlyKey));

		parameters.put(key, value);

		return parameters;
	}

	protected String getStatusMessage(@NotNull String groupName, int status, @Nullable String language) {
		int index = (status < 0) ? 0 : (SHORT.equals(groupName)) ? status : status + 1;
		MessageBundle localisedMessageBundle = getLocalisedMessageBundle(language);

		return localisedMessageBundle.optQueryString(SLASH + groupName + SLASH + index, localisedMessageBundle.getString(UNKNOWN_STATUS));
	}

	protected MessageBundle getLocalisedMessageBundle(String language) {
		return messageBundle.optBundle(language);
	}

}
