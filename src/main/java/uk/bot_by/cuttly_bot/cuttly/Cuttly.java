package uk.bot_by.cuttly_bot.cuttly;

import feign.QueryMap;
import feign.RequestLine;

import java.util.Map;

public interface Cuttly {

	@RequestLine("GET /api/api.php")
	String get(@QueryMap Map<String, String> parameters);

}
