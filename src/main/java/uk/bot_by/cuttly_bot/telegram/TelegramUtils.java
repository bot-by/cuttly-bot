package uk.bot_by.cuttly_bot.telegram;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static uk.bot_by.cuttly_bot.telegram.TelegramField.ChatID;
import static uk.bot_by.cuttly_bot.telegram.TelegramField.DisableNotification;
import static uk.bot_by.cuttly_bot.telegram.TelegramField.DisableWebPagePreview;
import static uk.bot_by.cuttly_bot.telegram.TelegramField.ParseMode;
import static uk.bot_by.cuttly_bot.telegram.TelegramField.ReplyToMessageID;
import static uk.bot_by.cuttly_bot.telegram.TelegramField.Text;

public class TelegramUtils {

	private static final String PARSE_MODE_HTML = "HTML";
	private static final String PARSE_MODE_MARKDOWN_V2 = "MarkdownV2";

	public static JSONObject disableNotification(JSONObject message) {
		message.put(DisableNotification.toString(), true);

		return message;
	}


	public static JSONObject disablePreview(JSONObject message) {
		message.put(DisableWebPagePreview.toString(), true);

		return message;
	}

	public static JSONObject makeMessage(long chatId, String text) {
		return messageBuilder()
				.add(ChatID, chatId)
				.add(Text, text)
				.build();
	}

	public static JSONObject makeReply(long chatId, long messageId, String text) {
		return messageBuilder()
				.add(ChatID, chatId)
				.add(ReplyToMessageID, messageId)
				.add(Text, text)
				.build();
	}

	public static JSONObject formatHypertext(JSONObject message) {
		message.put(ParseMode.toString(), PARSE_MODE_HTML);

		return message;
	}

	public static JSONObject formatMarkdown(JSONObject message) {
		message.put(ParseMode.toString(), PARSE_MODE_MARKDOWN_V2);

		return message;
	}

	private static MessageBuilder messageBuilder() {
		return new MessageBuilder();
	}

	private static class MessageBuilder {

		private static final String MESSAGE_FIELD_IS_NULL = "Message field cannot be null";

		private final Map<TelegramField, Object> fieldValues;

		MessageBuilder() {
			fieldValues = new HashMap<>();
		}

		MessageBuilder add(TelegramField telegramField, Object value) {
			Objects.requireNonNull(telegramField, MESSAGE_FIELD_IS_NULL);

			fieldValues.put(telegramField, value);

			return this;
		}

		JSONObject build() {
			return new JSONObject(fieldValues);
		}

	}

}
