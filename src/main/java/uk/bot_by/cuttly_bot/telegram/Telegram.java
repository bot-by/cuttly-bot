package uk.bot_by.cuttly_bot.telegram;

import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface Telegram {

	@RequestLine("POST /bot{token}/sendMessage")
	@Headers("Content-Type: application/json")
	String sendMessage(@Param("token") String token, String message);

}
