package uk.bot_by.cuttly_bot.telegram;

public enum TelegramField {

	ChatID("chat_id"),
	DisableNotification("disable_notification"),
	DisableWebPagePreview("disable_web_page_preview"),
	ParseMode("parse_mode"),
	ReplyToMessageID("reply_to_message_id"),
	Text("text");

	private final String fieldName;

	TelegramField(String fieldName) {
		this.fieldName = fieldName;
	}

	@Override
	public String toString() {
		return fieldName;
	}

}
