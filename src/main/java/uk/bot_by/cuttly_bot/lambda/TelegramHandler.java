package uk.bot_by.cuttly_bot.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyResponseEvent;
import feign.Feign;
import feign.slf4j.Slf4jLogger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.bot_by.cuttly_bot.cuttly.CuttlyHelper;
import uk.bot_by.cuttly_bot.telegram.Telegram;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static feign.Logger.Level.BASIC;
import static uk.bot_by.cuttly_bot.telegram.TelegramUtils.disablePreview;
import static uk.bot_by.cuttly_bot.telegram.TelegramUtils.makeMessage;
import static uk.bot_by.cuttly_bot.telegram.TelegramUtils.makeReply;

public class TelegramHandler implements RequestHandler<APIGatewayV2ProxyRequestEvent, APIGatewayV2ProxyResponseEvent> {

	private static final String DEFAULT_LANGUAGE = "DEFAULT_LANGUAGE";
	private static final String SHORTEN_LINK = "Shorten link: {} -> {}";
	private static final String WELCOME_TEXT =
			"\uD83C\uDDEC\uD83C\uDDE7 \uD83C\uDDFA\uD83C\uDDF8 Send or forward me a message: I will shorten every URL.\n" +
					"\uD83C\uDDF5\uD83C\uDDF1 Wyślij lub prześlij mi wiadomość: skrócę każdy adres URL.\n" +
					"\uD83C\uDDFA\uD83C\uDDE6 Надішли або перешли мені повідомлення: я скорочу кожну URL-адресу.\n" +
					"\uD83C\uDDF7\uD83C\uDDFA Отправь или перешли мне сообщение: я буду сокращать каждый URL.";
	// Response
	private static final String APPLICATION_JSON = "application/json";
	private static final String CONTENT_TYPE = "Content-Type";
	private static final int HTTP_OK = 200;
	private static final String OK = "Roger";
	private static final String TEXT_PLAIN = "text/plain";
	// Logger
	private static final String EMPTY_REQUEST = "Empty request from {}";
	private static final String FORWARDED_FOR = "x-forwarded-for";
	private static final String REQUEST_BODY = "Request body:\n{}";
	private static final String WRONG_REQUEST = "Wrong request from {}: {}\n{}";
	// Telegram, update
	private static final String BOT_COMMAND = "bot_command";
	private static final String CHAT = "chat";
	private static final String ENTITIES = "entities";
	private static final String FROM = "from";
	private static final String ID = "id";
	private static final String INLINE_QUERY = "inline_query";
	private static final String INLINE_RESULT = "chosen_inline_result";
	private static final String LANGUAGE_CODE = "language_code";
	private static final String LENGTH = "length";
	private static final String LINK = "url";
	private static final String MESSAGE = "message";
	private static final String OFFSET = "offset";
	private static final String TEXT = "text";
	private static final String TYPE = "type";
	// Commands
	private static final String COMMAND_SHORT = "/short";
	private static final String COMMAND_START = "/start";
	private static final String COMMAND_STATS = "/stats";
	private static final Collection<String> TEXT_WITH_ENTITIES = Arrays.asList(TEXT, ENTITIES);
	// Cuttly
	private static final String CUTTLY_API_KEY = "CUTTLY_API_KEY";
	private static final String CUTTLY_LINK = "url";
	private static final int CUTTLY_OK = 7;
	private static final String SHORT_LINK = "shortLink";
	private static final char SPACE = ' ';
	private static final String STATUS = "status";
	private static final String STATUS_MESSAGE = "statusMessage";
	// Telegram, send message
	private static final String TELEGRAM_API = "https://api.telegram.org";
	private static final String TELEGRAM_BOT_TOKEN = "TELEGRAM_BOT_TOKEN";
	private static final String MESSAGE_ID = "message_id";
	// Command
	private static final Function<JSONArray, Stream<JSONObject>> ENTITY_STREAM =
			(entities) -> StreamSupport.stream(entities.spliterator(), true)
					.map(object -> (JSONObject) object);
	private static final Predicate<Stream<JSONObject>> HAS_COMMANDS =
			(entityStream) -> entityStream
					.map(entity -> entity.get(TYPE))
					.anyMatch(BOT_COMMAND::equals);
	private static final Predicate<Stream<JSONObject>> ONLY_ONE_COMMAND =
			(entityStream) -> entityStream
					.map(entity -> entity.get(TYPE))
					.filter(BOT_COMMAND::equals)
					.count() == 1;
	private static final Predicate<Stream<JSONObject>> START_WITH_COMMAND =
			(entityStream) -> entityStream
					.filter(entity -> 0 == (entity.getInt(OFFSET)))
					.anyMatch(entity -> BOT_COMMAND.equals(entity.getString(TYPE)));
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private String cuttlyKey;

	@Override
	public APIGatewayV2ProxyResponseEvent handleRequest(@NotNull APIGatewayV2ProxyRequestEvent requestEvent, Context context) {
		Optional<APIGatewayV2ProxyResponseEvent> responseEvent = Optional.empty();

		if (null == requestEvent.getBody() || requestEvent.getBody().isBlank()) {
			logger.info(EMPTY_REQUEST, requestEvent.getHeaders().get(FORWARDED_FOR));
		} else {
			try {
				JSONObject update = new JSONObject(requestEvent.getBody());

				logger.debug(REQUEST_BODY, update.toString());
				responseEvent = (update.has(INLINE_QUERY)) ?
						processInlineMessage(update.getJSONObject(INLINE_QUERY)) :
						(update.has(MESSAGE) && update.getJSONObject(MESSAGE).keySet().containsAll(TEXT_WITH_ENTITIES)) ?
								processMessage(update.getJSONObject(MESSAGE)) :
								(update.has(INLINE_RESULT)) ?
										processInlineMessage(update.getJSONObject(INLINE_RESULT)) :
										responseEvent;
			} catch (JSONException exception) {
				logger.warn(WRONG_REQUEST, requestEvent.getHeaders().get(FORWARDED_FOR), exception.getMessage(),
						requestEvent.getBody());
			}
		}

		return responseEvent.orElse(responseOk());
	}

	Optional<APIGatewayV2ProxyResponseEvent> processInlineMessage(@NotNull JSONObject inlineQuery) {
		// TODO: add processing of inline messages, do not forget about user's language_code
		return Optional.empty();
	}

	Optional<APIGatewayV2ProxyResponseEvent> processMessage(@NotNull JSONObject message) {
		if (message.has(TEXT) && message.has(ENTITIES)) {
			String text = message.getString(TEXT);
			JSONArray entities = message.getJSONArray(ENTITIES);
			long chatId = message.getJSONObject(CHAT).getLong(ID);
			long messageId = message.getLong(MESSAGE_ID);
			String userLanguage = message.getJSONObject(FROM).optString(LANGUAGE_CODE);

			if (HAS_COMMANDS.test(ENTITY_STREAM.apply(entities))) {
				if (START_WITH_COMMAND.test(ENTITY_STREAM.apply(entities)) && ONLY_ONE_COMMAND.test(ENTITY_STREAM.apply(entities))) {
					return processCommand(chatId, messageId, text, entities, userLanguage);
				}
			} else {
				processText(chatId, messageId, text, entities, userLanguage);
			}
		}

		return Optional.empty();
	}

	Optional<APIGatewayV2ProxyResponseEvent> processCommand(long chatId, long messageId, @NotNull String text,
															@NotNull JSONArray entities, @Nullable String userLanguage) {
		Optional<APIGatewayV2ProxyResponseEvent> responseEvent = Optional.empty();
		String[] commandTokens = text.split("\\s");
		String command = commandTokens[0].toLowerCase();
		String firstParameter = (1 < commandTokens.length) ? commandTokens[1].toLowerCase() : null;
		String secondParameter = (2 < commandTokens.length) ? commandTokens[2].toLowerCase() : null;

		if ((2 == commandTokens.length || 3 == commandTokens.length) && COMMAND_SHORT.equals(command)) {
			JSONObject shortLinkReply = disablePreview(makeMessage(chatId, shortenLink(firstParameter, userLanguage, secondParameter)));
			getTelegram().sendMessage(getBotToken(), shortLinkReply.toString());
		} else if (2 == commandTokens.length && COMMAND_STATS.equals(command)) {
			// TODO: print stats
		} else if (1 == commandTokens.length && COMMAND_START.equals(command)) {
			JSONObject welcomeMessage = makeMessage(chatId, WELCOME_TEXT);
			getTelegram().sendMessage(getBotToken(), welcomeMessage.toString());
		}

		return responseEvent;
	}

	void processText(long chatId, long messageId, @NotNull String text, @NotNull JSONArray entities, @Nullable String userLanguage) {
		for (Object object : entities) {
			JSONObject entity = (JSONObject) object;

			if (LINK.equals(entity.getString(TYPE))) {
				long offset = entity.getLong(OFFSET), length = entity.getLong(LENGTH);
				String fullLink = text.codePoints()
						.skip(offset)
						.limit(length)
						.mapToObj(character -> (char) character)
						.map(Object::toString)
						.collect(Collectors.joining());
				JSONObject shortLinkReply = disablePreview(makeReply(chatId, messageId, shortenLink(fullLink, userLanguage)));

				getTelegram().sendMessage(getBotToken(), shortLinkReply.toString());
			}
		}
	}

	String getBotToken() {
		return System.getenv().get(TELEGRAM_BOT_TOKEN);
	}

	CuttlyHelper getCuttlyHelper() {
		return new CuttlyHelper(getCuttlyKey(), getDefaultLanguage());
	}

	String getCuttlyKey() {
		return System.getenv().get(CUTTLY_API_KEY);
	}

	String getDefaultLanguage() {
		return System.getenv().get(DEFAULT_LANGUAGE);
	}

	Telegram getTelegram() {
		return Feign.builder()
				.logger(new Slf4jLogger())
				.logLevel(BASIC)
				.target(Telegram.class, TELEGRAM_API);
	}

	APIGatewayV2ProxyResponseEvent getResponseEvent(@NotNull Object body) {
		APIGatewayV2ProxyResponseEvent responseEvent = new APIGatewayV2ProxyResponseEvent();

		responseEvent.setBody(body.toString());
		responseEvent.setHeaders(Collections.singletonMap(CONTENT_TYPE, APPLICATION_JSON));
		responseEvent.setIsBase64Encoded(false);
		responseEvent.setStatusCode(HTTP_OK);

		return responseEvent;
	}

	@NotNull
	APIGatewayV2ProxyResponseEvent responseOk() {
		APIGatewayV2ProxyResponseEvent responseEvent = getResponseEvent(OK);

		responseEvent.setHeaders(Collections.singletonMap(CONTENT_TYPE, TEXT_PLAIN));

		return responseEvent;
	}

	String shortenLink(String fullLink, String userLanguage) {
		return shortenLink(fullLink, userLanguage, null);
	}

	String shortenLink(String fullLink, String userLanguage, String name) {
		JSONObject cuttlyResponse = getCuttlyHelper().getShortLink(fullLink, userLanguage, name).getJSONObject(CUTTLY_LINK);
		String shortLinkReply = getShortLinkReply(cuttlyResponse);

		logger.debug(SHORTEN_LINK, fullLink, shortLinkReply);

		return shortLinkReply;
	}

	String getShortLinkReply(JSONObject cuttlyResponse) {
		StringBuilder replyMessage = new StringBuilder();

		if (CUTTLY_OK == cuttlyResponse.getInt(STATUS)) {
			replyMessage.append(cuttlyResponse.getString(SHORT_LINK));
		} else {
			replyMessage.append(cuttlyResponse.optString(SHORT_LINK)).append(SPACE);
			replyMessage.append(cuttlyResponse.getString(STATUS_MESSAGE));
		}

		return replyMessage.toString().trim();
	}

}
