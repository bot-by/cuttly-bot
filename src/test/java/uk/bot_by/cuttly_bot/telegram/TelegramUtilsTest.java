package uk.bot_by.cuttly_bot.telegram;

import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Tag("fast")
class TelegramUtilsTest {

	private JSONObject jsonObject;

	@DisplayName("Disabling notification.")
	@Test
	void disableNotification() {
		// when
		JSONObject message = TelegramUtils.disableNotification(jsonObject);

		// then
		assertThat(message.names(), contains("disable_notification"));
		assertTrue(message.getBoolean("disable_notification"));
	}

	@DisplayName("Disabling web-page preview.")
	@Test
	void disablePreview() {
		// when
		JSONObject message = TelegramUtils.disablePreview(jsonObject);

		// then
		assertThat(message.names(), contains("disable_web_page_preview"));
		assertTrue(message.getBoolean("disable_web_page_preview"));
	}

	@DisplayName("Make a message.")
	@Test
	void makeMessage() {
		// when
		JSONObject message = TelegramUtils.makeMessage(123, "qwerty");

		// then
		assertThat(message.names(), containsInAnyOrder("chat_id", "text"));
		assertThat(message.toMap().values(), containsInAnyOrder(123L, "qwerty"));
	}

	@DisplayName("Make a reply message.")
	@Test
	void makeReply() {
		// when
		JSONObject message = TelegramUtils.makeReply(123, 456, "qwerty");

		// then
		assertThat(message.names(), containsInAnyOrder("chat_id", "reply_to_message_id", "text"));
		assertThat(message.toMap().values(), containsInAnyOrder(123L, 456L, "qwerty"));
	}

	@DisplayName("Set HTML parse mode.")
	@Test
	void formatHypertext() {
		// when
		JSONObject message = TelegramUtils.formatHypertext(jsonObject);

		// then
		assertThat(message.names(), contains("parse_mode"));
		assertEquals("HTML", message.getString("parse_mode"));
	}

	@DisplayName("Set Markdown parse mode.")
	@Test
	void formatMarkdown() {
		// when
		JSONObject message = TelegramUtils.formatMarkdown(jsonObject);

		// then
		assertThat(message.names(), contains("parse_mode"));
		assertEquals("MarkdownV2", message.getString("parse_mode"));
	}

	@BeforeEach
	void setUp() {
		jsonObject = new JSONObject();
	}

}