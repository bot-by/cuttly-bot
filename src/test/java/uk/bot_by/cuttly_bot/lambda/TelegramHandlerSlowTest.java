package uk.bot_by.cuttly_bot.lambda;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import uk.bot_by.cuttly_bot.telegram.Telegram;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Tag("slow")
class TelegramHandlerSlowTest {

	private TelegramHandler handler;

	@DisplayName("Check that the bot could read Telegram bot token")
	@Test
	void getBotToken() {
		// when
		String actualToken = handler.getBotToken();

		// then
		assertEquals("123456:qwerty", actualToken, "Telegram bot token is reading from TELEGRAM_BOT_TOKEN");
	}

	@DisplayName("Check that the bot could read Cuttly key")
	@Test
	void getCuttlyKey() {
		// when
		String actualKey = handler.getCuttlyKey();

		// then
		assertEquals("1q2w3e4r5t6y7", actualKey, "Telegram bot token is reading from CUTTLY_API_KEY");
	}

	@DisplayName("Check that Telegram client is created")
	@Test
	void getTelegram() {
		// when
		Telegram telegram = handler.getTelegram();

		// then
		assertNotNull(telegram, "Telegram interface");
	}

	@BeforeEach
	void setUp() {
		handler = new TelegramHandler();
	}

}