package uk.bot_by.cuttly_bot.lambda;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;
import com.amazonaws.services.lambda.runtime.ClientContext;
import com.amazonaws.services.lambda.runtime.CognitoIdentity;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyResponseEvent;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.slf4j.LoggerFactory;
import uk.bot_by.cuttly_bot.cuttly.CuttlyHelper;
import uk.bot_by.cuttly_bot.telegram.Telegram;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.StringContains.containsString;
import static org.hamcrest.text.IsBlankString.blankOrNullString;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Tag("fast")
class TelegramHandlerFastTest {

	@Mock
	private Appender<ILoggingEvent> appender;
	@Captor
	private ArgumentCaptor<LoggingEvent> captorLoggingEvent;
	@Captor
	private ArgumentCaptor<String> captorString;
	@Captor
	private ArgumentCaptor<JSONObject> captorUpdate;
	@Mock
	private CuttlyHelper cuttlyHelper;
	@Mock
	private JSONObject cuttlyResponse;
	@Mock
	private Telegram telegram;

	@Spy
	private TelegramHandler handler;

	private Context context;
	private APIGatewayV2ProxyRequestEvent requestEvent;
	private APIGatewayV2ProxyResponseEvent responseEvent;

	@DisplayName("Handles Telegram request, wrong request.")
	@Test
	void handleWrongRequest() {
		// given
		requestEvent.setBody("name1=value1&name2=value2");

		doReturn(responseEvent).when(handler).responseOk();

		// when
		APIGatewayV2ProxyResponseEvent actualResponseEvent = handler.handleRequest(requestEvent, context);

		// then
		verify(handler, never()).processInlineMessage(isA(JSONObject.class));
		verify(handler, never()).processMessage(isA(JSONObject.class));
		verify(handler).responseOk();
		verify(appender).doAppend(captorLoggingEvent.capture());

		final List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();

		assertAll("Write to the log a warning message",
				() -> assertEquals(1, loggingEvents.size()),
				() -> assertEquals(
						"Wrong request from 127.0.0.1: " +
								"A JSONObject text must begin with '{' at 1 [character 2 line 1]\n" +
								"name1=value1&name2=value2",
						loggingEvents.get(0).getFormattedMessage()),
				() -> assertEquals(Level.WARN, loggingEvents.get(0).getLevel()));

		assertAll("Process Telegram request and return response",
				() -> assertNotNull(actualResponseEvent, "Response event is not null"),
				() -> assertEquals(responseEvent, actualResponseEvent, "Response event passed"));
	}

	@DisplayName("Handle empty request, write IP address to info log.")
	@ParameterizedTest
	@NullAndEmptySource
	@ValueSource(strings = "  ")
	void handleEmptyRequest(String body) {
		// given
		requestEvent.setBody(body);

		doReturn(responseEvent).when(handler).responseOk();

		// when
		APIGatewayV2ProxyResponseEvent actualResponseEvent = handler.handleRequest(requestEvent, context);

		// then
		verify(handler, never()).processInlineMessage(isA(JSONObject.class));
		verify(handler, never()).processMessage(isA(JSONObject.class));
		verify(handler).responseOk();
		verify(appender).doAppend(captorLoggingEvent.capture());

		final List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();

		assertAll("Write to the log an info message",
				() -> assertEquals(1, loggingEvents.size()),
				() -> assertEquals("Empty request from 127.0.0.1", loggingEvents.get(0).getFormattedMessage()),
				() -> assertEquals(Level.INFO, loggingEvents.get(0).getLevel()));

		assertAll("Process Telegram request and return response",
				() -> assertNotNull(actualResponseEvent, "Response event is not null"),
				() -> assertEquals(responseEvent, actualResponseEvent, "Response event passed"));
	}

	@DisplayName("Process message.")
	@Test
	void processMessage() {
		// given
		requestEvent.setBody(
				"{ message: { from: { first_name: Jack }, text: qwerty, entities: [ { type: italic, offset: 0, length: 5 } ] } }");

		doReturn(Optional.empty()).when(handler).processMessage(isA(JSONObject.class));

		// when
		handler.handleRequest(requestEvent, context);

		// then
		verify(handler, never()).processInlineMessage(isA(JSONObject.class));
		verify(handler).processMessage(captorUpdate.capture());

		final JSONObject actualUpdate = captorUpdate.getValue();

		assertEquals("{\"entities\":[{\"offset\":0,\"length\":5,\"type\":\"italic\"}]," +
				"\"from\":{\"first_name\":\"Jack\"},\"text\":\"qwerty\"}", actualUpdate.toString(), "Message");
	}

	@DisplayName("Process inline query.")
	@ParameterizedTest
	@ValueSource(strings = {"inline_query", "chosen_inline_result"})
	void processInlineQuery(String keyName) {
		// given
		requestEvent.setBody("{ " + keyName + ": { query: qwerty }}");

		doReturn(Optional.empty()).when(handler).processInlineMessage(isA(JSONObject.class));

		// when
		handler.handleRequest(requestEvent, context);

		// then
		verify(handler, never()).processMessage(isA(JSONObject.class));
		verify(handler).processInlineMessage(captorUpdate.capture());

		final JSONObject actualUpdate = captorUpdate.getValue();

		assertEquals("{\"query\":\"qwerty\"}", actualUpdate.toString(), "Message");
	}

	@DisplayName("Process unknown message.")
	@Test
	void processUnknownUpdate() {
		// given
		requestEvent.setBody("{ edited_message: { text: qwerty }}");

		// when
		handler.handleRequest(requestEvent, context);

		// then
		verify(handler, never()).processMessage(isA(JSONObject.class));
		verify(handler, never()).processInlineMessage(isA(JSONObject.class));
	}

	@DisplayName("Message without text is ignored.")
	@Test
	void messageWithoutTextIsIgnored() {
		//String message = "{ text: qwerty, entities: [ { type: italic, offset: 0, length: 5 } ] } }";
		// then
		String message = "{ 'document': { " +
				"'file_id': 'AwADBAADbXXXXXXXXXXXGBdhD2l6_XX', " +
				"'file_name': 'Testfile.pdf', " +
				"'mime_type': 'application/pdf', " +
				"'file_size': '536392' }}";

		// when
		handler.processMessage(new JSONObject(message));

		// then
		verify(handler, never()).processCommand(anyLong(), anyLong(), anyString(), isA(JSONArray.class), anyString());
		verify(handler, never()).processText(anyLong(), anyLong(), anyString(), isA(JSONArray.class), anyString());
	}

	@DisplayName("Message without entities is ignored.")
	@Test
	void messageWithoutEntitiesIsIgnored() {
		// given
		String message = "{ text: qwerty }}";

		// when
		handler.processMessage(new JSONObject(message));

		// then
		verify(handler, never()).processCommand(anyLong(), anyLong(), anyString(), isA(JSONArray.class), anyString());
		verify(handler, never()).processText(anyLong(), anyLong(), anyString(), isA(JSONArray.class), anyString());
	}

	@DisplayName("Command message that starts with this command is processed.")
	@Test
	void oneCommandMessageIsProcessed() {
		// given
		String message = "{ 'message_id': 123, 'chat': { 'id': 33 }, 'text': '/start qwerty', " +
				"'entities': [ { 'type': 'bot_command', 'offset': 0, 'length': 5 } ], " +
				"'from': { 'first_name': 'Jack' } }}";

		doReturn(Optional.empty()).when(handler).processCommand(anyLong(), anyLong(), anyString(), isA(JSONArray.class), anyString());

		// when
		handler.processMessage(new JSONObject(message));

		// then
		verify(handler).processCommand(eq(33L), eq(123L), eq("/start qwerty"), isA(JSONArray.class), eq(""));
		verify(handler, never()).processText(anyLong(), anyLong(), anyString(), isA(JSONArray.class), anyString());
	}

	@DisplayName("Command message that starts with text is ignored.")
	@Test
	void mixedCommandMessageIsIgnored() {
		// given
		String message = "{ 'message_id': 123, 'chat': { 'id': 33 }, 'text': 'abc /start qwerty', " +
				"'entities': [ { 'type': 'bot_command', 'offset': 4, 'length': 9 } ], " +
				"'from': { 'first_name': 'Jack' } }}";

		// when
		handler.processMessage(new JSONObject(message));

		// then
		verify(handler, never()).processCommand(anyLong(), anyLong(), anyString(), isA(JSONArray.class), anyString());
		verify(handler, never()).processText(anyLong(), anyLong(), anyString(), isA(JSONArray.class), anyString());
	}

	@DisplayName("Multi-command message is ignored.")
	@Test
	void multiCommandMessageIsIgnored() {
		// given
		String message = "{ 'message_id': 123, 'chat': { 'id': 33 }, 'text': '/start /stop', " +
				"'entities': [ " +
				"{ 'type': 'bot_command', 'offset': 0, 'length': 5 }, " +
				"{ 'type': 'bot_command', 'offset': 7, 'length': 11 }, ], " +
				"'from': { 'first_name': 'Jack' } }}";

		// when
		handler.processMessage(new JSONObject(message));

		// then
		verify(handler, never()).processCommand(anyLong(), anyLong(), anyString(), isA(JSONArray.class), anyString());
		verify(handler, never()).processText(anyLong(), anyLong(), anyString(), isA(JSONArray.class), anyString());
	}

	@DisplayName("Text (non-command) message is processed.")
	@Test
	void textMessageIsProcessed() {
		// given
		String message = "{ 'message_id': 123, 'chat': { 'id': 33 }, 'text': 'go google.com', " +
				"'entities': [ { 'type': 'url', 'offset': 3, 'length': 10 } ], " +
				"'from': { 'first_name': 'Jack' } }}";

		doNothing().when(handler).processText(anyLong(), anyLong(), anyString(), isA(JSONArray.class), anyString());

		// when
		handler.processMessage(new JSONObject(message));

		// then
		verify(handler, never()).processCommand(anyLong(), anyLong(), anyString(), isA(JSONArray.class), anyString());
		verify(handler).processText(eq(33L), eq(123L), eq("go google.com"), isA(JSONArray.class), eq(""));
	}

	@DisplayName("Find some urls in text message then shorten then")
	@Test
	void textMessageWithTwoLinks() {
		// given
		String text = "go to google.com then to cutt.ly";
		String entities = "[ { 'type': 'url', 'offset': 6, 'length': 10 }, { 'type': 'url', 'offset': 25, 'length': 7 } ]";

		doReturn("http://short.link/abc").when(handler).shortenLink(anyString(), anyString());
		doReturn(telegram).when(handler).getTelegram();
		doReturn("qwerty").when(handler).getBotToken();
		doReturn("abc").when(telegram).sendMessage(anyString(), anyString());

		// when
		handler.processText(123, 937, text, new JSONArray(entities), "aa");

		// then
		verify(handler).shortenLink(eq("google.com"), eq("aa"));
		verify(handler).shortenLink(eq("cutt.ly"), eq("aa"));
		verify(telegram, times(2)).sendMessage(eq("qwerty"), captorString.capture());

		assertAll("Reply message with a short link",
				() -> assertThat(captorString.getValue(), containsString("http://short.link/abc")),
				() -> assertThat(captorString.getValue(), containsString("\"reply_to_message_id\":937")));
	}

	@DisplayName("Find some urls in text message then shorten then")
	@Test
	void textMessageWithoutAnyLinks() {
		// given
		String text = "go to google then to cutt";
		String entities = "[ { 'type': 'italic', 'offset': 6, 'length': 6 }, { 'type': 'bold', 'offset': 21, 'length': 4 } ]";

		// when
		handler.processText(123, 937, text, new JSONArray(entities), "aa");

		// then
		verify(handler, never()).shortenLink(anyString(), anyString());
		verify(telegram, never()).sendMessage(anyString(), anyString());
	}

	@DisplayName("Returns HTTP 200.")
	@Test
	void responseOk() {
		// when
		APIGatewayV2ProxyResponseEvent responseEvent = handler.responseOk();

		// then
		assertAll("Return HTTP 200 with correct content type and not empty body",
				() -> assertEquals(200, responseEvent.getStatusCode(), "HTTP status code"),
				() -> assertThat("Response body", responseEvent.getBody(), not(blankOrNullString())));
	}

	@DisplayName("Shorten link")
	@Test
	void shortenLink() {
		// given
		doReturn("test response").when(handler).getShortLinkReply(isA(JSONObject.class));

		// when
		String actualShortLink = handler.shortenLink("abc", "aa");

		// then
		verify(cuttlyHelper).getShortLink(eq("abc"), eq("aa"), isNull());
		verify(cuttlyResponse).getJSONObject(eq("url"));
		verify(handler).getCuttlyHelper();
		verify(handler).getShortLinkReply(eq(cuttlyResponse));
		verify(appender).doAppend(captorLoggingEvent.capture());

		final List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();

		assertAll("Write to the log an debug message",
				() -> assertEquals(1, loggingEvents.size()),
				() -> assertEquals("Shorten link: abc -> test response", loggingEvents.get(0).getFormattedMessage()),
				() -> assertEquals(Level.DEBUG, loggingEvents.get(0).getLevel()));

		assertEquals("test response", actualShortLink, "Short link");
	}

	@DisplayName("Shorten named link")
	@Test
	void shortenNamedLink() {
		// given
		doReturn("test response").when(handler).getShortLinkReply(isA(JSONObject.class));

		// when
		String actualShortLink = handler.shortenLink("abc", "aa", "xyz");

		// then
		verify(cuttlyHelper).getShortLink(eq("abc"), eq("aa"), eq("xyz"));
		verify(cuttlyResponse).getJSONObject(eq("url"));
		verify(handler).getCuttlyHelper();
		verify(handler).getShortLinkReply(eq(cuttlyResponse));
		verify(appender).doAppend(captorLoggingEvent.capture());

		final List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();

		assertAll("Write to the log an debug message",
				() -> assertEquals(1, loggingEvents.size()),
				() -> assertEquals("Shorten link: abc -> test response", loggingEvents.get(0).getFormattedMessage()),
				() -> assertEquals(Level.DEBUG, loggingEvents.get(0).getLevel()));

		assertEquals("test response", actualShortLink, "Short link");
	}

	@DisplayName("Built string from Cuttly's JSON response, link was shorten")
	@Test
	void shortLinkOkReply() {
		// given
		when(cuttlyResponse.getInt(anyString())).thenReturn(7);
		when(cuttlyResponse.getString(anyString())).thenReturn("test response");

		// when
		String actualShortLinkReply = handler.getShortLinkReply(cuttlyResponse);

		// then
		verify(cuttlyResponse).getInt(eq("status"));
		verify(cuttlyResponse).getString(eq("shortLink"));

		assertEquals("test response", actualShortLinkReply, "Short link reply");
	}

	@DisplayName("Built string from Cuttly's JSON response, link was not shorten")
	@Test
	void shortLinkNotOkReply() {
		// given
		when(cuttlyResponse.getInt(anyString())).thenReturn(2);
		when(cuttlyResponse.optString(anyString())).thenReturn("");
		when(cuttlyResponse.getString(anyString())).thenReturn("status message");

		// when
		String actualShortLinkReply = handler.getShortLinkReply(cuttlyResponse);

		// then
		verify(cuttlyResponse).getInt(eq("status"));
		verify(cuttlyResponse).optString("shortLink");
		verify(cuttlyResponse).getString("statusMessage");

		assertEquals("status message", actualShortLinkReply, "Short link reply");
	}

	@DisplayName("Built string from Cuttly's JSON response, link was not shorten")
	@Test
	void shortLinkOkReplyWithMessage() {
		// given
		when(cuttlyResponse.getInt(anyString())).thenReturn(2);
		when(cuttlyResponse.optString(anyString())).thenReturn("short link");
		when(cuttlyResponse.getString(anyString())).thenReturn("status message");

		// when
		String actualShortLinkReply = handler.getShortLinkReply(cuttlyResponse);

		// then
		verify(cuttlyResponse).getInt(eq("status"));
		verify(cuttlyResponse).optString("shortLink");
		verify(cuttlyResponse).getString("statusMessage");

		assertEquals("short link status message", actualShortLinkReply, "Short link reply");
	}

	@DisplayName("Create the shorten helper with API key")
	@Test
	void getCuttlyHelper() {
		// given
		doCallRealMethod().when(handler).getCuttlyHelper();
		doReturn("en").when(handler).getDefaultLanguage();

		// when
		CuttlyHelper helper = handler.getCuttlyHelper();

		// then
		verify(handler).getCuttlyKey();

		assertNotNull(helper, "Helper is not null");
	}

	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);

		context = new TestContext();
		requestEvent = new APIGatewayV2ProxyRequestEvent();
		responseEvent = new APIGatewayV2ProxyResponseEvent();

		requestEvent.setHttpMethod("POST");
		requestEvent.setHeaders(Collections.singletonMap("x-forwarded-for", "127.0.0.1"));

		((Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME)).addAppender(appender);

		doReturn(cuttlyHelper).when(handler).getCuttlyHelper();
		doReturn("qwerty").when(handler).getCuttlyKey();
		when(cuttlyHelper.getShortLink(anyString(), any(), any())).thenReturn(cuttlyResponse);
		when(cuttlyResponse.getJSONObject(anyString())).thenReturn(cuttlyResponse);
	}

	private static class TestContext implements Context {

		@Override
		public String getAwsRequestId() {
			return null;
		}

		@Override
		public String getLogGroupName() {
			return null;
		}

		@Override
		public String getLogStreamName() {
			return null;
		}

		@Override
		public String getFunctionName() {
			return null;
		}

		@Override
		public String getFunctionVersion() {
			return null;
		}

		@Override
		public String getInvokedFunctionArn() {
			return null;
		}

		@Override
		public CognitoIdentity getIdentity() {
			return null;
		}

		@Override
		public ClientContext getClientContext() {
			return null;
		}

		@Override
		public int getRemainingTimeInMillis() {
			return 0;
		}

		@Override
		public int getMemoryLimitInMB() {
			return 0;
		}

		@Override
		public LambdaLogger getLogger() {
			return null;
		}

	}

}