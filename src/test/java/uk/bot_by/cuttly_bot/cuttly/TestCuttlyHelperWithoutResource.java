package uk.bot_by.cuttly_bot.cuttly;

public class TestCuttlyHelperWithoutResource extends CuttlyHelper {

	public TestCuttlyHelperWithoutResource() {
		super("xyz", "en");
	}

	public TestCuttlyHelperWithoutResource(String cuttlyKey, String defaultLanguage) {
		super(cuttlyKey, defaultLanguage);
	}

}
