package uk.bot_by.cuttly_bot.cuttly;

public class TestCuttlyHelper extends CuttlyHelper {

	public TestCuttlyHelper() {
		super("xyz", "en");
	}

	public TestCuttlyHelper(String cuttlyKey, String defaultLanguage) {
		super(cuttlyKey, defaultLanguage);
	}

}
