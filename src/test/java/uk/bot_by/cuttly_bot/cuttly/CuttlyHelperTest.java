package uk.bot_by.cuttly_bot.cuttly;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.ThrowingSupplier;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.collection.IsMapContaining.hasEntry;
import static org.hamcrest.collection.IsMapWithSize.aMapWithSize;
import static org.hamcrest.core.StringStartsWith.startsWith;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Tag("fast")
class CuttlyHelperTest {

	@Mock
	private Appender<ILoggingEvent> appender;
	@Mock
	private Cuttly cuttly;
	@Captor
	private ArgumentCaptor<LoggingEvent> captorLoggingEvent;
	@Captor
	private ArgumentCaptor<Map<String, String>> captorParameters;

	private CuttlyHelper cuttlyHelper;

	@DisplayName("Shorten link with name")
	@Test
	void getShortLinkWithName() {
		// given
		when(cuttly.get(anyMap())).thenReturn("{ 'url': { 'status': '7', 'shortLink': 'http://short.link/abc' }}");

		// when
		JSONObject actualResponse = cuttlyHelper.getShortLink("http://abc.com/", "aa", "abc");

		// then
		verify(cuttlyHelper).getStatusMessage(eq("short"), eq(7), eq("aa"));
		verify(cuttly).get(captorParameters.capture());

		Map<String, String> parameters = captorParameters.getValue();

		assertAll("Query parameters",
				() -> assertThat("Parameter map has key, full link and link name", parameters, aMapWithSize(3)),
				() -> assertThat("Parameter names", parameters.keySet(), containsInAnyOrder("key", "short", "name")),
				() -> assertThat("Parameter values", parameters.values(), containsInAnyOrder("abc", "http://abc.com/", "qwerty")));
		assertAll("Cuttly response",
				() -> assertEquals("OK - the link has been shortened.",
						actualResponse.getJSONObject("url").getString("statusMessage"), "Response has a status message"));
	}

	@DisplayName("Shorten link without name")
	@Test
	void getShortLink() {
		// given

		when(cuttly.get(anyMap())).thenReturn("{ 'url': { 'status': '7', 'shortLink': 'http://short.link/abc' }}");

		// when
		JSONObject actualResponse = cuttlyHelper.getShortLink("http://abc.com/", "aa", null);

		// then
		verify(cuttlyHelper).getStatusMessage(eq("short"), eq(7), eq("aa"));
		verify(cuttly).get(captorParameters.capture());

		Map<String, String> parameters = captorParameters.getValue();

		assertAll("Query parameters",
				() -> assertThat("Parameter map has key, full link", parameters, aMapWithSize(2)),
				() -> assertThat("Parameter names", parameters.keySet(), containsInAnyOrder("key", "short")),
				() -> assertThat("Parameter values", parameters.values(), containsInAnyOrder("qwerty", "http://abc.com/")));
		assertAll("Cuttly response",
				() -> assertEquals("OK - the link has been shortened.",
						actualResponse.getJSONObject("url").getString("statusMessage"), "Response has a status message"));
	}

	@DisplayName("Get statistics")
	@Test
	void getStatistics() {
		// given
		when(cuttly.get(anyMap())).thenReturn("{ stats: { status: 1, clicks: 7 }}");

		// when
		JSONObject actualResponse = cuttlyHelper.getStatistics("http://abc.com/", "aa");

		// then
		verify(cuttly).get(captorParameters.capture());

		Map<String, String> parameters = captorParameters.getValue();

		assertAll("Query parameters",
				() -> assertThat("Parameter map has key and short link", parameters, aMapWithSize(2)),
				() -> assertThat("Parameter names", parameters.keySet(), containsInAnyOrder("key", "stats")),
				() -> assertThat("Parameter values", parameters.values(), containsInAnyOrder("qwerty", "http://abc.com/")));
		assertAll("Cuttly Response",
				() -> assertEquals("This link exists and the data has been downloaded.",
						actualResponse.getJSONObject("stats").getString("statusMessage"), "Response has a status message"));
	}

	@DisplayName("Parameters with the predefined Cuttly API key.")
	@Test
	void getParameters() {
		// when
		Map<String, String> parameters = cuttlyHelper.getParameters("qwerty", "xyz");

		// then
		assertThat(parameters, hasEntry("qwerty", "xyz"));
	}

	@DisplayName("If a language is not found it uses default.")
	@ParameterizedTest
	@ValueSource(strings = "ru")
	@EmptySource
	@NullSource
	void languageIsUnknownOrNull(String language) {
		// when
		TestCuttlyHelper cuttlyHelper = assertDoesNotThrow((ThrowingSupplier<TestCuttlyHelper>) TestCuttlyHelper::new,
				"The resource file is named after a helper class");

		String statusMessage = cuttlyHelper.getStatusMessage("short", 1, language);

		// then
		assertEquals("Last message", statusMessage, "The language is not found so it uses default");
	}

	@DisplayName("The resource JSON-file is loaded.")
	@Test
	void messagesAreLoaded() {
		// when
		TestCuttlyHelper cuttlyHelper = assertDoesNotThrow((ThrowingSupplier<TestCuttlyHelper>) TestCuttlyHelper::new,
				"The resource file is named after a helper class");
		String statusMessage = cuttlyHelper.getStatusMessage("anotherGroup", 1, "uk");

		// then
		assertEquals("Трете повідомлення", statusMessage, "The status message is found");
	}

	@DisplayName("Negative status is error. It uses first message (index = 0).")
	@Test
	void errorMessage() {
		// given
		TestCuttlyHelper cuttlyHelper = new TestCuttlyHelper();

		//
		String statusMessage = cuttlyHelper.getStatusMessage("short", -11, "uk");

		// then
		assertEquals("Перше повідомлення", statusMessage, "The status message is not found");
	}

	@DisplayName("Unknown status")
	@Test
	void unknownStatus() {
		// given
		TestCuttlyHelper cuttlyHelper = new TestCuttlyHelper();

		//
		String statusMessage = cuttlyHelper.getStatusMessage("short", 111, "uk");

		// then
		assertEquals("+++", statusMessage, "The status message is not found");
	}

	@DisplayName("Unknown group")
	@Test
	void unknownGroup() {
		// given
		TestCuttlyHelper cuttlyHelper = new TestCuttlyHelper();

		//
		String statusMessage = cuttlyHelper.getStatusMessage("short2", 7, "uk");

		// then
		assertEquals("+++", statusMessage, "The status message is not found");
	}

	@DisplayName("Just check that Cuttly's instance is created")
	@Test
	void getCuttly() {
		// given
		doCallRealMethod().when(cuttlyHelper).getCuttly();

		// when & then
		assertNotNull(cuttlyHelper.getCuttly(), "Cuttly HTTP client");
	}

	@DisplayName("Default language does not exist")
	@ParameterizedTest
	@ValueSource(strings = "aa")
	@EmptySource
	void defaultLanguageDoesNotExist(String defaultLanguage) {
		// when
		RuntimeException exception = assertThrows(RuntimeException.class, () -> new CuttlyHelper("qwerty", defaultLanguage));

		// then
		assertThat("Exception message", exception.getMessage(), startsWith("Bundle is not found: "));
	}

	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);

		cuttlyHelper = spy(new CuttlyHelper("qwerty", "en"));
		doReturn(cuttly).when(cuttlyHelper).getCuttly();

		((Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME)).addAppender(appender);
	}

}